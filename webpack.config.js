'use strict';

var webpack = require('webpack');

module.exports.getConfig = function(type) {

  var isDev = type === 'dev';

  var config = {
    entry: {
      main: [
        __dirname + '/node_modules/babel-core/browser-polyfill.js',
        './app/js/main.js'
      ]
    },
    output: {
      path: __dirname,
      filename: '[name].min.js'
    },
    resolve: {
      modulesDirectories: ['node_modules'],
      extensions: ['', '.js', '.jsx']
    },
    module: {
      loaders: [{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader?stage=1'
      }]
    }
  };

  if (isDev) {
    config.debug = true;
    config.devtool = 'eval';
  } else {
    config.plugins = [ new webpack.optimize.UglifyJsPlugin() ];
  }

  return config;
}