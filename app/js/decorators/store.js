/**
 * decorators/store @lq 2015/08/04
*/

'use strict';

export default {
	getInitialState() {
		return this.state;
	},

	setState(state={}) {
		Object.assign(this.state, state);
		this.trigger(this.state);
	}
};