/**
 * decorators/autobind @lq 2015/07/31
 *
 * usage: 
 *
 * @reactMixin.decorate(xxx)
 * class xxx
*/

'use strict';

export default function(...methods) {
	return {
		componentWillMount() {
      methods.forEach((method) => {
        this[method] = this[method].bind(this);
      });
    }
  };
}