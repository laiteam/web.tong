export default [
	{
		id: 0,
		deposit: 50,
		date: '2015年5月15日',
		status: 0,
		reason: ''
	},
	{
		id: 1,
		deposit: 50,
		date: '2015年6月15日',
		status: 2,
		reason: ''
	},
	{
		id: 2,
		deposit: 50,
		date: '2015年7月15日',
		status: 1,
		reason: ''
	},
	{
		id: 3,
		deposit: 50,
		date: '2015年8月1日',
		status: -1,
		reason: '水桶损坏/遗失'
	}
];