/**
 * definitions @lq 2015/08/02
*/

'use strict';

const definitions = {
	APIConf: {
		host: 'http://123.57.139.153:9000',
		ver: 'v1',
		method: 'POST',
		type: 'jsonp',
		contentType: 'application/json'
	},
	API: {
		verification: '/user/verification',
		login: '/user/login',
		orders: '/user/orders',
		order: '/user/orders/detail',
		cancel: '/user/orders/cancel',
		address: '/user/address',
		ordering: '/user/ordering',
		wares: '/packman/wares'
	},
	HASH: {
		salt: 'yto.app.hash',
		min: 8,
		characters: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123567890'
	},
	JWT: {
		secret: 'yto.app.jwt'
	},
	COOKIE: {
		getInfinite() {
			let expires = new Date().getFullYear() + 200; // 200 年后过期
			return new Date(String(expires)).toUTCString();
		}
	},
	// store state status
	STATUS: {
		init: 0,
		process: 1,
		done: 2,
		fail: -1
	},
	// document title
	TITLE: {
		'default': '来一桶',
		'login': '登录',
		'profile': '个人中心',
		'orders': '全部订单',
		'order/\d*': '订单详情',
		'bucket-deposits': '水桶押金'
	},
	COUNT: 10000, // 每页显示条数
	SEARCH_COUNT: 15,
	ORDER_STATUS: {
		cancel: -1,
		unpay: 0,
		undelivery: 1,
		delivering: 2,
		done: 10
	},
	ORDERING_STATUS: {
		init: 0, 
		has: 1, // 有水站
		rest: 2, // 水站休息
		no: 3 // 无水站
	},
	STORAGE_KEY: {
		uid: 'uid',
		phone: 'phone',
		order: 'order'
	},
	AMAP: {
		tableid: '55adbd97e4b0a76fce4e9685',
		searchRange: 2500
	},
	DEBOUNCE: {
		searchAddress: 800 // ms
	}
};

export default definitions;
