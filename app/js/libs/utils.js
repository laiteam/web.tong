/**
 * libs/utils @lq 2015/08/04
*/

'use strict';

function supportLocalStorage() {
	return !!window.localStorage;
}

function timestr2timestamp (timestr) {
	let date = new Date();
	let year = date.getFullYear();
	let month = date.getMonth() + 1;
	let day = date.getDate();

	return Date.parse(`${year}/${month}/${day} ${timestr}`);
}

export default { 
	supportLocalStorage,
	timestr2timestamp
};