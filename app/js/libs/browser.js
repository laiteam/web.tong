/**
 * libs/browser @lq 2015/08/04
*/

'use strict';

export default function () {
	let ua = window.navigator.userAgent.toLowerCase();

	// devices: ios/android
	let device = ua.includes('android') ? 'android' : 'ios';

	// wechat: bool
	let wechat = ua.includes('micromessenger') ? true : false;

	return [ device, wechat ];
}