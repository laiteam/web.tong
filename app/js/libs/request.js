/**
 * libs/request @lq 2015/08/
*/

'use strict';

import reqwest from 'reqwest';

import { APIConf, STORAGE_KEY } from '../definitions';
import storage from '../libs/storage';

function genUrl(url) {
	return `${APIConf.host}/api/${APIConf.ver}${url}`;
}

function req(url, params={}) {
  // let uId = storage.get(STORAGE_KEY.uid, { hashids: true });
  let uId = '504696854806663168'; // beta 正伟 uid

  params['sign'] = +new Date();
  if (uId) params['user_id'] = uId;

	try {
		return reqwest({
			type: APIConf.type,
			method: APIConf.method,
			contentType: APIConf.contentType,
			url: genUrl(url),
			data: params
		});
	} catch(err) {
		console.error(err);
		return false;
	}
}

function ajax(generator, options={}) {
	let g = generator();

  function go(result) {
    return result.value.then(
    	(res) => {
    		let { error_code: code, error_message: msg } = res;

    		if (msg) {
    			console.log(msg);
          options.alert && alert(msg);

    			g.next([false, { code, msg }]);
    		} else g.next([true, res]);
    	},
    	(err) => {
    		console.error(err);
    		//g.next([false, { code: -1 }]);
        location.href = '#/error';
    	}
    );
  }

  go(g.next());
}

export default { ajax, req };
