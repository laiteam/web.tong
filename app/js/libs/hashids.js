/**
 * libs/hashids @lq 2015/08/03
*/

'use strict';

import Hashids from 'hashids';

import { HASH } from '../definitions';

let hashids = new Hashids(HASH.salt, HASH.min, HASH.characters);

function encode(id='') {
	return hashids.encodeHex(id);
}

function decode(hash='') {
	return hashids.decodeHex(hash);
}

export default { encode, decode };