/**
 * libs/jwt @lq 2015/08/04
*/

'use strict';

import jwt from 'jwt-simple';
import { JWT } from '../definitions';

function encode(payload={}) {
	return jwt.encode(payload, JWT.secret);
}

function decode(token='') {
	return jwt.decode(token, JWT.secret);
}

export default { encode, decode };