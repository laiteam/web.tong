/**
 * 云图 API @lq 2015/08/08
*/

import { AMAP, SEARCH_COUNT } from '../definitions';

// 地物点搜索
function placeSearch(cityCode, keywords, cb) {
  let MSearch;

  AMap.service(['AMap.PlaceSearch'], () => {
    MSearch = new AMap.PlaceSearch({
      pageSize: SEARCH_COUNT,
      pageIndex: 1,
      city: cityCode
    });

    // 关键字查询
    MSearch.search(keywords, (stat, res) => {
      if (stat == 'complete' && res.info == 'OK') {
        // 有结果
        cb(true, res);
      } else {
        // 无结果
        cb(false);
      }
    });
  });
}

// 周边检索
function searchNearBy(lng, lat, cb) {
  let center = new AMap.LngLat(lng, lat);

  AMap.service(['AMap.CloudDataSearch'], () => {
    let aMap = new AMap.CloudDataSearch(AMAP.tableid, {
      sortrule: '_distance' // 按距离排序
    });

    // 周边检索
    aMap.searchNearBy(center, AMAP.searchRange, (stat, res) => {
      //cb(false); return; // 模拟没有水站
      if (stat == 'complete') {
        // 检索成功
        if (!res.datas.length) cb(false); // 无匹配
        else cb(true, res.datas[0]) // 返回距离最近的第一个匹配项
      } else {
        // 检索失败
        cb(false);
      }
    });
  });
}

// 定位
function getPosition(cb) {
  let geolocation;

  // 加载地图，调用浏览器定位服务
  let map = new AMap.Map('mapContainer');

  map.plugin('AMap.Geolocation', function () {
    geolocation = new AMap.Geolocation({
      enableHighAccuracy: true, // 是否使用高精度定位，默认:true
      timeout: 10000,           // 超过10秒后停止定位，默认：无穷大
      showButton: false,        // 显示定位按钮，默认：true
      showMarker: false,        // 定位成功后在定位到的位置显示点标记，默认：true
      showCircle: false,        // 定位成功后用圆圈表示定位精度范围，默认：true
      panToLocation: true,      // 定位成功后将定位到的位置作为地图中心点，默认：true
    });

    map.addControl(geolocation);

    // 定位成功
    AMap.event.addListener(geolocation, 'complete', (res) => {
      cb(true, res);
    });

    // 定位失败
    AMap.event.addListener(geolocation, 'error', (res) => {
      cb(false);
    });
  });

  try {
    geolocation.getCurrentPosition();
  } catch (err) {
    setTimeout(() => {
      geolocation.getCurrentPosition();
    }, 1000);
  }
}

// 根据经纬度获取Poi
function getPoi(lon, lat, cb) {
  AMap.service(['AMap.Geocoder'], () => { 
    // 加载地理编码
    let geocoder = new AMap.Geocoder({
      radius: 10,
      extensions: "all"
    });

    geocoder.getAddress(new AMap.LngLat(lon, lat), (stat, res) => {
      if (stat == 'complete') {
        let regeocode = res.regeocode;
        let { pois, addressComponent } = regeocode;
        let poi = addressComponent.building || (addressComponent.township + addressComponent.streetNumber);
        
        if (pois.length) {
          let _poi = pois[0].address;
          if (_poi) poi = _poi;
        }

        let cityCode = addressComponent.citycode;
        
        cb(true, poi, cityCode);
      }
      else cb(false);
    });
  });
}

export default { placeSearch, searchNearBy, getPosition, getPoi };
