/**
 * libs/date @lq 2015/08/04
*/

'use strict';

export default function (t, options={}) {
	let date = new Date(t*1000);
	let year = date.getFullYear();
	let month = date.getMonth() + 1;
	let day = date.getDate();
	let hour = date.getHours();
	let minute = (m=date.getMinutes()) => {
		return (m < 10 ? '0' : '') + m;
	}();

	return { year, month, day, hour, minute };
}