/**
 * libs/storage @lq 2015/08/03
*/

'use strict';

import { encode as hashEncode, decode as hashDecode } from './hashids';
import { encode as jwtEncode, decode as jwtDecode } from './jwt';
import cookie from './cookie';
import { supportLocalStorage } from './utils';

let MEDIA = {
	localStorage: 0,
	cookie: 1
};

function _check(options) {
	if (options.hashids == undefined && options.jwt == undefined) {
		alert('storage options err');
		return false;
	}

	return true;
}

function remove(key) {
	let media = supportLocalStorage() ? MEDIA.localStorage : MEDIA.cookie;
	if (media == MEDIA.localStorage) window.localStorage.removeItem(key);
	else cookie(key, null);
}

function get(key, options={}) {
	if (!_check(options)) return;

	let media = supportLocalStorage() ? MEDIA.localStorage : MEDIA.cookie;
	let payload = media == MEDIA.localStorage 
		? (window.localStorage.getItem(key) || '')
		: (cookie(key) || '');

	if (options.hashids) return hashDecode(payload);
	if (options.jwt) return jwtDecode(payload);
}

function set(key, value, options={}) {
	if (!_check(options)) return;

	let media = supportLocalStorage() ? MEDIA.localStorage : MEDIA.cookie;

	if (options.hashids) value = hashEncode(String(value));
	if (options.jwt) value = jwtEncode(value);

	if (media == MEDIA.localStorage) window.localStorage.setItem(key, value);
	else cookie(key, value, options);
}

export default { remove, get, set };

// export default function(key, value, options={}) {
// 	let media = supportLocalStorage() ? MEDIA.localStorage : MEDIA.cookie;

// 	if (arguments.length < 2) return get(media, key);
// 	else {
// 		if (!value) remove(media, key);
// 		else set(media, key, value, options)
// 	}
// }