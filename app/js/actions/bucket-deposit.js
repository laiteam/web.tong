/**
 * actions/bucket-deposit.js @lq 2015/07/20
*/

'use strict';

import Reflux from 'reflux';

let actions = Reflux.createActions([
	'fetch' // 获取水桶押金列表
]);

export default actions;