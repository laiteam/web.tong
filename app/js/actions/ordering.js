/**
 * actions/ordering @lq 2015/08/07
*/

'use strict';

import Reflux from 'reflux';

export default Reflux.createActions([
  'fetchAddress',
  'searchAddress',
  'positioning',
  'searchPackman',
  'fetchWares',
  'select',
  'reset',
  'setStatus',
  'setSubmit'
]);
