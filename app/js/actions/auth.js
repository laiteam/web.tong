/**
 * actions/auth.js @lq 2015/07/22
*/

'use strict';

import Reflux from 'reflux';

export default Reflux.createActions([
	'fetchCaptcha',
	'login'
]);