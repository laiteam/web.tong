/**
 * actions/order @lq 2015/08/04
*/

'use strict';

import Reflux from 'reflux';

export default Reflux.createActions([
	'fetchAll',
	'fetchOne',
	'cancel'
]);