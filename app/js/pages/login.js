/**
 * pages/login.js @lq 2015/07/20
*/

'use strict';

import React from 'react';
import { Navigation } from 'react-router';
import reactMixin from 'react-mixin';
import Reflux from 'reflux';

import { STATUS } from '../definitions';
import store from '../stores/auth';
import actions from '../actions/auth';

@reactMixin.decorate(Navigation)
@reactMixin.decorate(Reflux.ListenerMixin)
export default class Login extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			login: STATUS.init,
			telValid: false,
			fetchCaptcha: false,
			formValid: false,
			countdown: this.props.countdown
		};
	}

	componentDidMount() {
		this.listenTo(store, (state) => {
			// 登录失败
			if (state.login == STATUS.fail) {
				this.setCaptcha('');
				this.setState({ login: STATUS.fail });
			}

			// 登录成功
			if (state.login == STATUS.done) {
				window.location = '/#/ordering';
				//this.transitionTo('ordering'); // 貌似有的时候加载 amap 会出问题
			}
		});
	}

	getTel() {
		return React.findDOMNode(this.refs.tel).value.trim();
	}

	getCaptcha() {
		return React.findDOMNode(this.refs.captcha).value.trim();
	}

	setCaptcha(value) {
		React.findDOMNode(this.refs.captcha).value = value;
	}

	telValid() {
		return /^\d{11}$/.test(this.getTel());
	}

	captchaValid() {
		return /^\d{4}$/.test(this.getCaptcha());
	}

	countdown() {
		let st = window.setInterval(() => {
			if (this.state.countdown == 1) {
				window.clearInterval(st);
				this.setState({ 
					countdown: this.props.countdown,
					fetchCaptcha: false
				});
			} else this.setState({ countdown: --this.state.countdown });
		}, 1000);
	}

	focusTel(e) {
		if (e.target.className != 'pull-right ui-btn ui-btn-xs') this.refs.tel.getDOMNode().focus();
	}

	fetchCaptcha(e) {
		if (!this.state.telValid) return;

		actions.fetchCaptcha(this.getTel());

		this.countdown();
		this.setState({ fetchCaptcha: true});
	}

	valid() {
		this.setState({
			telValid: this.telValid(),
			formValid: this.telValid() && this.captchaValid()
		});
	}

	login(e) {
		e.preventDefault(); // 阻止表单提交

		this.setState({ login: STATUS.process });
		actions.login(this.getTel(), this.getCaptcha());
	}

	render() {
		return (
			<div>
				<p>为方便送水小哥联系你，请验证手机号码</p>
				<form className="ui-form" onSubmit={this.login.bind(this)}>
					<ul className="ui-list-group">
						<li onTouchEnd={this.focusTel.bind(this)}>
							<input type="text" className="ui-form-ctrl" style={{ width: '100px' }} placeholder="手机号码" maxLength="11" required disabled={this.state.fetchCaptcha} ref="tel" onKeyUp={this.valid.bind(this)} />
							{
								!this.state.fetchCaptcha
								 	? <a href="javascript:;" className="pull-right ui-btn ui-btn-xs" disabled={!this.state.telValid} onClick={this.fetchCaptcha.bind(this)}>获取验证码</a>
								 	: <a href="javascript:;" className="pull-right ui-btn ui-btn-xs" disabled>
											<span>
												<span ref="countdown">{this.state.countdown}</span> 秒后重新获取
											</span>
										</a>
							}
						</li>
						<li>
							<input type="text" className="ui-form-ctrl ui-form-ctrl-block" placeholder="验证码" maxLength="4" autoCapitalize="off" autoCorrect="off" autoComplete="off" ref="captcha" onKeyUp={this.valid.bind(this)} />
						</li>
					</ul>
					<div className="ui-form-actions">
						{
							this.state.login == STATUS.init || this.state.login == STATUS.fail
								? <button type="submit" className="ui-btn ui-btn-block" disabled={!this.state.formValid} ref="submit">登录</button>
								: <a href="javascript:;" className="ui-btn ui-btn-block" disabled>登录中 ...</a>
						}
					</div>
				</form>
			</div>
		);
	}
}

Login.defaultProps = { countdown: 60 };
Login.propTypes = { countdown: React.PropTypes.number };