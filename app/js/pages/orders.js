/**
 * pages/orders @lq 2015/08/01
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';
import Reflux from 'reflux';

import { STATUS } from '../definitions';
import store from '../stores/order';
import actions from '../actions/order';
import Order from '../components/order';

@reactMixin.decorate(Reflux.connectFilter(store, 'orders', (state) => { return state.orders }))
export default class Orders extends React.Component {

	constructor(props) {
		super(props);
	}

	componentWillMount() {
		actions.fetchAll();
	}

	render() {
		let orders = this.state.orders;

		if (orders == STATUS.process) {
			return <p className="text-center text-muted">努力加载中 ...</p> 
		}

		return (
			<div className="ui-mt-10">
			{
				!orders.length
				  ? <div className="text-center text-muted">暂无订单</div>
				  : orders.map((order, idx) => {
							return <Order key={idx} {...order} />
						})
			}
			</div>
		);
	}
}