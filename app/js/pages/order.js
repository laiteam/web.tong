/**
 * pages/order @lq 2015/08/01
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';
import { Link, Navigation } from 'react-router';
import Reflux from 'reflux';

import { STATUS, ORDER_STATUS } from '../definitions';
import autobind from '../decorators/autobind';
import Order_ from '../components/order';
import orders from '../data/orders';
import store from '../stores/order';
import actions from '../actions/order';

@reactMixin.decorate(Navigation)
@reactMixin.decorate(Reflux.ListenerMixin)
@reactMixin.decorate(autobind('deposit', 'cancel'))
export default class Order extends React.Component {

	constructor(props) {
		super(props);

		this.state = { order: STATUS.process };
	}

	componentWillMount() {
		actions.fetchOne(this.props.params.id);
	}

	componentDidMount() {
		this.listenTo(store, (state) => {
			if (state.order == 'cancel') this.transitionTo('orders');
			else this.setState({ order: state.order });
		});
	}

	deposit(e) {
		this.state.deposited
			? e.preventDefault()
			: confirm(`确定补交水桶押金 ¥${this.state.deposit}?`) && this.setState({ deposited: true });
	}

	cancel() {
		confirm("取消订单后，已支付金额会在48小时内原路退回。确定要取消订单？") && actions.cancel(this.state.order.id);
	}

	render() {
		let order = this.state.order;

		if (order == STATUS.process) {
			return <p className="text-center text-muted">努力加载中 ...</p>;
		}

		if (order == STATUS.fail) {
			this.transitionTo('error');
		}

		return (
			<div className="ui-mt-10">
				<div className="ui-line-behind-title">
					<span>明细</span>
				</div>
				<Order_ {...order} />
				<div className="ui-line-behind-title ui-mt-20">
					<span>操作</span>
				</div>
				{
					order.status != ORDER_STATUS.done
						? <div className="ui-list-group text-center">
								{/*<a href="javascript:;" className="ui-list-group-item" disabled={order.deposited} onClick={this.deposit}>补交水桶押金&nbsp;(¥{order.deposit})</a>*/}
								{
									order.status != ORDER_STATUS.cancel || orders.status != ORDER_STATUS.delivering
										? <a href="javascript:;" className="ui-list-group-item" onClick={this.cancel}>取消订单</a>
										: null
								}
								<a href={`tel:${order.packman.phone}`} className="ui-list-group-item">联系水站</a>
							</div>
						: null
				}	
				<Link to="ordering" params={{ status: 'has' }} className="ui-btn ui-btn-block ui-btn-inverse ui-mt-10">返回首页</Link>
			</div>
		);
	}
}