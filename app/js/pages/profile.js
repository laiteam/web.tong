/**
 * pages/profile @lq 2015/08/01
*/

'use strict';

import React from 'react';
import { Link } from 'react-router';

import storage from '../libs/storage';
import store from '../stores/auth';

export default class Profile extends React.Component {

	constructor() {
		super();
	}

	logout(e) {
		if (!confirm('你确定要退出吗？')) e.preventDefault();
		else store.logout();
	}

	render() {
		let phone = storage.get('phone', { hashids: true });

		return (
			<div className="ui-mt-10">
				<div className="ui-panel">
					<div className="ui-panel-body">{`${phone.slice(0,3)}****${phone.slice(7)}`}</div>
				</div>
				<div className="ui-list-group ui-list-actions ui-mt-10">
					<Link to="orders" className="ui-list-group-item">
						全部订单<i className="pull-right ui-icon ui-icon-chevron-right"></i>
					</Link>
					{/*
					<Link to="bucket-deposits" className="ui-list-group-item">
						水桶押金<i className="pull-right ui-icon ui-icon-chevron-right"></i>
					</Link>
					*/}
				</div>
				<Link to="login" className="ui-btn ui-btn-block ui-btn-transparent ui-mt-10" onClick={this.logout}>退出登录</Link>
				<img src="img/slogan.png" alt="slogan" className="ui-img-slogan" />
			</div>
		);
	}
}
