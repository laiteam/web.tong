/**
 * pages/error @lq 2015/08/05
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';
import { Navigation } from 'react-router';

@reactMixin.decorate(Navigation)
export default class Error extends React.Component {
	constructor() {
		super();
	}

	render() {
		return (
			<p className="text-center" style={{ marginTop: '50%' }}>
				T_T 页面加载失败&nbsp;
				<a href="javascript:;" className="text-primary" onClick={() => this.goBack()}>刷新重试</a>
			</p>
		);	
	}
}