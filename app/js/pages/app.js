/**
 * pages/app.js @lq 2015/07/20
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';
import { RouteHandler, Navigation } from 'react-router';
import DocumentTitle from 'react-document-title';

import browser from '../libs/browser';
import { TITLE } from '../definitions';
import store from '../stores/auth';

@reactMixin.decorate(Navigation)
export default class App extends React.Component {

	constructor() {
		super();
	}

	componentDidUpdate() {
		let [ device, wechat ] = browser();

		// hack在微信等webview中无法修改document.title的情况
		if (device == 'ios' && wechat) {
			let iframe = document.createElement('iframe');
			iframe.style.visibility = 'hidden';
			iframe.src = 'img/blank.png'; // 任何一个可访问的路径，不指定无法触发 load

			iframe.addEventListener('load', () => {
				window.setTimeout(() => {
					iframe.removeEventListener('load');
					iframe.parentNode.removeChild(iframe);
				}, 0);
			});
			
			document.body.appendChild(iframe);
		}
	}

	getDocTitle(path) {
		let title = TITLE.default;

		for (let k in TITLE) {
			if (new RegExp(k).test(path)) title = TITLE[k];
		}

		return title;
	}

	render() {
		let path = location.hash.slice(2);
		
		if (!store.checkLogined() && path != 'error') this.transitionTo('login');
		else {
			if (location.hash.slice(2) == 'login') this.transitionTo('ordering'); // 如果 #/ 则跳转
		}

		let title = this.getDocTitle(path);

		return (
			<DocumentTitle title={title}>
				<div className="container">
					<RouteHandler />
				</div>
			</DocumentTitle>
		);
	}
}