/**
 * pages/ordering.js @lq 2015/07/20
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';
import { Link } from 'react-router';
import Reflux from 'reflux';

import { STATUS, ORDERING_STATUS } from '../definitions';
import autobind from '../decorators/autobind';
import amap from '../libs/amap';
import actions from '../actions/ordering';
import store from '../stores/ordering';
import OrderingHeader from '../components/ordering-header';
import OrderingAddress from '../components/ordering-address';
import OrderingOption from '../components/ordering-option';
import OrderingSwitcher from '../components/ordering-switcher';
import OrderingPrice from '../components/ordering-price';
import OrderingHint from '../components/ordering-hint';

@reactMixin.decorate(Reflux.connect(store))
@reactMixin.decorate(autobind('switcher', 'setPrice', 'submit'))
export default class Ordering extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			positioning: false,
			payType: 'wechat',
			price: 20,
			count: 1
		};
	}	

	componentWillMount() {
		// 获取默认地址列表
		actions.fetchAddress();
	}

	componentDidMount() {}

	componentWillUpdate(nextProps, { addresses, defaultAddress, packman, wares, lng, lat }) {
		if (defaultAddress == ORDERING_STATUS.init) {
			// 无默认地址，开始定位
			console.log('无默认地址，开始定位');
			actions.positioning();
		} 

		if (defaultAddress && packman == STATUS.init) {
			// 有默认地址，搜索附近水站
			console.log('有默认地址，搜索附近水站');
			actions.searchPackman();
		}

		if (typeof packman == 'object' && wares == STATUS.init) {
			// 获取商品列表
			console.log('获取商品列表');
			actions.fetchWares(packman.packman_id);
		}
	}

	// 切换支付类型
	switcher(payType) {
		this.setState({ payType: payType });
	}

	// 设置总价
	setPrice({ price=this.state.price, count=1 }) {
		this.setState({
			price: price,
			count: count
		});
	}

	// 下单
	submit(e) {
		alert('开发中！急毛~');
		e.preventDefault();
		// this.props.params.status == ORDERING_STATUS.no
		// 	? e.preventDefault()
		// 	: alert('下单成功，转向订单列表页 ...');
	}

	render() {
		let { addresses, defaultAddress, searchResults, status, submit, packman, wares, price, count, payType } = this.state;

		return (
			<div className="ui-pos-re">
				<OrderingHeader status={status} packman={packman} />
				<OrderingAddress status={status} addresses={addresses} defaultAddress={defaultAddress} searchResults={searchResults} />
				<OrderingOption status={status} wares={wares} setPrice={this.setPrice} count={this.state.count} packman={packman} />
				{
					status == ORDERING_STATUS.has || status == ORDERING_STATUS.rest
						?	<div className="text-center ui-mt-20 ui-un">
								<OrderingSwitcher packman={packman} payType={payType} toggle={this.switcher} />
								<div className="ui-mv-10">
									{payType == 'wechat' ? <OrderingPrice total={price*count}  /> : null}
									<OrderingHint packman={packman} payType={payType} />
								</div>
							</div>
						: null
				}
				<Link to="order/:id" params={{ id: 0 }} className="ui-btn ui-btn-block ui-mt-10 ui-un" disabled={status == ORDERING_STATUS.no || status == ORDERING_STATUS.init} onClick={this.submit}>
					{() => {
						switch (submit) {
							case ORDERING_STATUS.init: return '加载中...';
							case ORDERING_STATUS.has: return '立即叫水';
							case ORDERING_STATUS.no: return '暂无可服务水站';
							case ORDERING_STATUS.rest: return '预约送水';
						}
					}()}
				</Link>
			</div>
		);
	}
}

Ordering.defaultProps = {
};

Ordering.propTypes = {
};