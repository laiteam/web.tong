/**
 * pages/bucket-deposits.js @lq 2015/07/20
*/

'use strict';

import React from 'react';
import DocumentTitle from 'react-document-title';

import BucketDeposit from '../components/bucket-deposit';
import bucketDeposits from '../data/bucket-deposits';

export default class BucketDeposits extends React.Component {
	
	constructor() {
		super();
	}

	render() {
		return (
			<DocumentTitle title="水桶押金">
				<div className="ui-mt-10">
					<p className="text-center text-note">如果您遇到任何问题，欢迎使用 微信回复</p>
					{
						bucketDeposits.map((bucketDeposit, idx) => {
							return <BucketDeposit key={idx} {...bucketDeposit} />
						})
					}
				</div>
			</DocumentTitle>
		);
	}
}