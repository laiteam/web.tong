/**
 * routes.js @lq 2015/07/20
*/

'use strict';

import React from 'react';
import { Route, DefaultRoute, NotFoundRoute } from 'react-router';

import App from './pages/app';
import Login from './pages/login';
import Ordering from './pages/ordering';
import Order from './pages/order';
import Orders from './pages/orders';
import Profile from './pages/profile';
import BucketDeposits from './pages/bucket-deposits';
import Error from './pages/error';

export default (
	<Route name="app" path="/" handler={App}>
		<DefaultRoute handler={Ordering} />
		<Route name="login" handler={Login} />
		<Route name="ordering" handler={Ordering} />
		<Route name="order/:id" handler={Order} />
		<Route name="orders" handler={Orders} />
		<Route name="profile" handler={Profile} />
		<Route name="bucket-deposits" handler={BucketDeposits} />
		<Route name="error" handler={Error} />
	</Route>
);