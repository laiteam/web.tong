/**
 * components/modal @lq 2015/07/25
*/

'use strict';

import React from 'react';
import classNames from 'classnames';

export default class Modal extends React.Component {

	constructor(props) {
		super(props);
  }

	render() {
		let classes = classNames({
			'ui-modal': true,
			'in': this.props.show
		});

		return (
    	<div className={classes}>
				<div className="ui-modal-dialog">
					<div className="ui-modal-content text-left">
						<h4 className="text-bold text-em ui-mv-0">什么是水桶押金？</h4>
						<p className="text-sm">水桶押金是针对“无空水桶”的用户在下单时收取的押金费用，费用为50元</p>
						<h4 className="text-bold text-em ui-mb-0">如何退水桶押金？</h4>
						<p className="text-sm">请在服务号下点击菜单“我的“ > “水桶押金” 即可申请退款</p>
					</div>
					<div className="ui-modal-action">
						<a href="javascript:;" className="ui-btn ui-btn-block ui-mt-10" onClick={() => this.props.toggle(false)}>我知道了</a>
					</div>
				</div>
			</div>
		);
	}
}

Modal.defaultProps = { 
	show: false,
	toggle: null
};

Modal.propTypes = { 
	show: React.PropTypes.bool,
	toggle: React.PropTypes.func
};