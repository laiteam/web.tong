/**
 * components/ordering-hint @lq 2015/07/27
 *
 * props.payType: wechat 微信 / ticket 水票
 * 微信支付，并且用户第一次叫水，显示 ”水桶押金 ...“ 文案
 * 纸质水票，显示 “仅支持 xxx水票 ... “ 文案
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';

import autobind from '../decorators/autobind';

import Modal from '../components/modal';

@reactMixin.decorate(autobind('modal'))
export default class OrderingHint extends React.Component {

	constructor() {
		super();

		this.state = { modal: false };
	}

	// for child component `this` context
	modal(state) {
		this.setState({ modal: state });
	}

	render() {
		return (
			<div>
				{
					this.props.payType == 'wechat'
						? <a className="text-muted text-sm" href="javascript:;" onClick={() => this.modal(true)}>
								水桶押金 ¥50，此押金随时可退&nbsp;&nbsp;
								<i className="ui-icon ui-icon-chevron-right" style={{ marginTop: '-2px' }}></i>
							</a>
						: <span className="text-muted text-sm ui-mv-20">
								仅支持 <span className="text-default">{this.props.packman._name}</span> 水票，请认真核对
							</span>
				}
				<Modal show={this.state.modal} toggle={this.modal} />
			</div>
		);
	}
}

OrderingHint.propTypes = { 
	packman: React.PropTypes.any,
	payType: React.PropTypes.string 
};

OrderingHint.defaultProps = { 
	packman: {},
	payType: 'wechat' 
};