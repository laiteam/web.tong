/**
 * components/ordering-price @lq 2015/07/27
 *
 * props.total {number} 总价
*/

'use strict';

import React from 'react';

export default class OrderingPrice extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<span>
				共计：<span className="ui-va-m text-xl text-danger">¥ {this.props.total}</span>
			</span>
		);
	}
}

OrderingPrice.propTypes = { total: React.PropTypes.number }
OrderingPrice.defaultProps = { total: 0 }