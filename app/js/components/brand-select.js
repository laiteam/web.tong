/**
 * components/brand-select @lq 2015/07/27
*/

'use strict';

import React from 'react';

import ModalSelect from './modal-select';

export default class BrandSelect extends ModalSelect {

	constructor() {
		super();
	}	

	parse() {
		let brands = {};

		for (let ware of this.props.wares) {
			brands[ware.name] = ware.name + '<span class="pull-right">¥ ' + ware.price + '</span>';
		}

		return brands;
	}

	getPrice(brandName) {
		for (let ware of this.props.wares) {
			if (brandName == ware.name) return ware.price;
		}
	}

	done() {
		let brand = SpinningWheel.getSelectedValues().keys[0];
		let price = this.getPrice(brand);
		
		this.props.set(brand, price);
	}

	slot() {
		SpinningWheel.addSlot(this.parse(this.brands), 'left');		

		SpinningWheel.setCancelAction(() => {
			this.close();
		});

		SpinningWheel.setDoneAction(() => {
			this.done();
		});

		SpinningWheel.open('选择品牌');
	}
}

BrandSelect.defaultProps = {
	wares: []
};

BrandSelect.propTypes = {
	wares: React.PropTypes.any
};