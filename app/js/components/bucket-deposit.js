/**
 * components/bucket-deposit @lq 2015/08/02
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';
import classNames from 'classnames';

import autobind from '../decorators/autobind';

@reactMixin.decorate(autobind('refund'))
export default class BucketDeposit extends React.Component {

	constructor(props) {
		super(props);

		this.state = { status: props.status };
	}

	refund() {
		confirm('你确定要退还水桶押金吗？') && this.setState({ status: 2 });
	}

	render() {
		let classes = classNames({
			'ui-panel': true,
			'ui-panel-muted': Math.abs(this.props.status) == 1,
			'ui-mt-10': !this.props.key
		});

		let actionClasses = classNames({
			'ui-bucket-deposit-action': true,
			'ui-with-status': this.state.status == 1
		});

		return (
			<div className={classes}>
				<div className="ui-panel-body">
					<div className="ui-bucket-deposit">
						<span className="ui-bucket-deposit-price">¥ {this.props.deposit}</span>
						<div className="ui-bucket-deposit-meta">
							<span>水桶押金</span>
							<div className="ui-bucket-deposit-time">{this.props.date}</div>
						</div>
						<div className={actionClasses}>
						{
							() => {
								switch (this.state.status) {
									case 0: return <a href="javascript:;" className="ui-btn ui-btn-sm" onClick={this.refund}>退押金</a>;
									case 2: 
										return (
											<div>
												<a href="tel:400-400-400" className="ui-btn ui-btn-sm">联系水站</a>
												<div className="ui-bucket-deposit-status">（处理中）</div>
											</div>
										);
									case 1: return <a className="ui-btn ui-btn-sm" disabled>押金已退</a>;
									case -1: return <a className="ui-btn ui-btn-sm" disabled>押金作废</a>;
								}
							}()
						}
						</div>
					</div>
				</div>
				{this.props.reason ? <div className="ui-panel-footer text-sm">原因：{this.props.reason}</div> : null}
			</div>
		);
	}
}

BucketDeposit.defaultProps = {
	id: 0,
	deposit: 0,
	date: '',
	status: 0,
	reason: ''
};

BucketDeposit.propTypes = {
	id: React.PropTypes.number,
	deposit: React.PropTypes.number,
	date: React.PropTypes.string,
	status: React.PropTypes.number,
	reason: React.PropTypes.string
};