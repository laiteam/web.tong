 /**
 * components/ordering-option.js @lq 2015/07/22
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';
import classNames from 'classnames';

import autobind from '../decorators/autobind';
import { STATUS, ORDERING_STATUS } from '../definitions';
import OrderingCount from './ordering-count'
import BrandSelect from './brand-select';
import TimeSelect from './time-select';

@reactMixin.decorate(autobind('selectBrand', 'setBrand', 'selectTime', 'setTime'))
export default class OrderingOption extends React.Component {

	constructor() {
		super();

		this.state = {
			brandToggle: false,
			brand: false,
			count: 1,
			timeToggle: false,
			time: false
		};
	}

	componentWillReceiveProps({ wares }) {
		if (!this.state.brand && wares != STATUS.init && wares.length) {
			// 设置默认选中商品
			console.log('设置默认选中商品');
			let { name, price } = wares[0];
			this.setBrand(name, price);
		}

		if (wares == ORDERING_STATUS.init && this.state.brand) {
			// 重置
			this.setState({ 
				brand: false,
				count: 1 
			});
		}
	}

	selectBrand(state) {
		this.setState({ brandToggle: state });
	}

	setBrand(brand, price) {
		this.setState({
			brandToggle: false,
			brand: brand
		});

		this.props.setPrice({ price: price });
	}

	selectTime(state) {
		this.setState({ timeToggle: state });
	}

	setTime(time) {
		this.setState({
			timeToggle: false,
			time: time
		});
	}

	render() {
		let classes = classNames({
			'text-muted': !this.state.brand
		});

		let { status, wares, packman } = this.props;

		return (
			<div className="ui-mt-10 ui-un">
				<div className="ui-list-group">
					<a href="javascript:;" className="ui-list-group-item" onClick={() => this.selectBrand(true)}>
						<span className="text-muted">品牌</span>
						<span className="pull-right">
						{
							this.state.brand
								? <span>
										{this.state.brand}&nbsp;&nbsp;<i className="ui-icon ui-icon-chevron-right"></i>
									</span>
								: <span className="text-muted">
									{() => {
										switch (status) {
											case ORDERING_STATUS.init: return '加载中...';
											case ORDERING_STATUS.no: return '暂无';
										}
									}()}
									</span>
						}
						</span>
					</a>
					<div className="ui-list-group-item">
						<OrderingCount status={status} setPrice={this.props.setPrice} count={this.props.count} />
					</div>
					<a href="javascript:;" className="ui-list-group-item" onClick={() => this.selectTime(true)}>
						<span className="text-muted">时间</span>
						<span className="pull-right">
						{() => {
							switch (status) {
								case ORDERING_STATUS.init: return <span className="text-muted">加载中...</span>;
								case ORDERING_STATUS.no: return <span className="text-muted">暂无</span>;
								case ORDERING_STATUS.has: return (
									<span>
										{this.state.time || '现在'}&nbsp;&nbsp;<i className="ui-icon ui-icon-chevron-right"></i>
									</span>
								);
								case ORDERING_STATUS.rest: return (
									<span>
										{this.state.time || `明天 ${packman.start_at}`}&nbsp;&nbsp;<i className="ui-icon ui-icon-chevron-right"></i>
									</span>
								);
							}
						}()}
						</span>
					</a>
				</div>
				{
					wares != STATUS.init && wares.length
						? <BrandSelect wares={this.props.wares} show={this.state.brandToggle} toggle={this.selectBrand} set={this.setBrand} />
						: null
				}
				{
					status != ORDERING_STATUS.init && status != ORDERING_STATUS.no && packman
						?	<TimeSelect show={this.state.timeToggle} toggle={this.selectTime} set={this.setTime} start={packman.start_at} end={packman.end_at} status={status} />
						: null
				}
			</div>
		);
	}
}

OrderingOption.propTypes = {
	status: React.PropTypes.number,
	wares: React.PropTypes.any,
	setPrice: React.PropTypes.func,
	count: React.PropTypes.number,
	packman: React.PropTypes.any
};

OrderingOption.defaultProps = {
	status: ORDERING_STATUS.init,
	wares: [],
	setPrice: null,
	count: 1,
	packman: {}
}
