/**
 * components/ordering-count @lq 2015/07/27
*/

'use strict';

import React from 'react';

import { ORDERING_STATUS } from '../definitions';

export default class OrderingCount extends React.Component {

	constructor(props) {
		super(props);

		this.state = { count: props.count };
	}

	add() {
		let status = this.props.status;
		if (status == ORDERING_STATUS.init || status == ORDERING_STATUS.no) return

		let count = ++this.state.count;

		this.setState({ count: count });
		this.props.setPrice({ count: count });
	}

	minus() {
		let status = this.props.status;
		if (status == ORDERING_STATUS.init || status == ORDERING_STATUS.no) return

		if (this.state.count < 2) return;

		let count = --this.state.count;

		this.setState({ count: count });
		this.props.setPrice({ count: count });
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ count: nextProps.count });
	}

	render() {
		return (
			<div>
				<span className="text-muted">数量</span>
				<div className="pull-right ui-num-select">
					<a href="javascript:;" onTouchStart={this.minus.bind(this)}>
						<i className="ui-icon ui-icon-minus"></i>
					</a>
					<span className="ui-num-inner">{this.state.count}</span>
					<a href="javascript:;" onTouchStart={this.add.bind(this)}>
						<i className="ui-icon ui-icon-plus"></i>
					</a>
				</div>
			</div>
		);
	}
}

OrderingCount.defaultProps = { 
	status: ORDERING_STATUS.init,
	setPrice: null 
};
OrderingCount.propTypes = { 
	status: React.PropTypes.number,
	setPrice: React.PropTypes.func 
};