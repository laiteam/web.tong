/**
 * components/ordering-address.js @lq 2015/07/22
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';
import classNames from 'classnames';

import autobind from '../decorators/autobind';
import amap from '../libs/amap';
import { STATUS, ORDERING_STATUS } from '../definitions';
import actions from '../actions/ordering';

export default class OrderingAddress extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			select: false,
			search: false,
			poi: '',
			detail: ''
		};

		this.last = null;
	}

	componentDidMount() {}

	componentWillReceiveProps({ defaultAddress }) {
		if (defaultAddress && !this.state.search && !this.state.search) {
			let state = { poi: defaultAddress.poi };

			if (!this.state.detail) state['detail'] = defaultAddress.address;
			
			this.setState(state);
		}
	}

	toggle(selectState, searchState) {
		let state = {
			select: selectState,
			search: searchState
		};

		if (this.state.search && !this.state.poi.trim()) {
			state['poi'] = this.props.defaultAddress.poi;
			state['detail'] = this.props.defaultAddress.address;
		}

		this.setState(state);
	}

	setAddress(poi, detail='', lng, lat) {
		let state = {
			select: false,
			search: false
		};

		if (poi != undefined) state['poi'] = poi;
		if (detail != undefined) state['detail'] = detail;

		this.setState(state);
		actions.select(poi, lng, lat);
	}

	positioning() {
		this.setState({ 
			poi: '',
			detail: '',
			select: false 
		});

		actions.reset();
	}

	poiInput(e) {
		this.setState({ poi: e.target.value })
	}

	detailInput(e) {
		this.setState({ detail: e.target.value })
	}

	search() {
		this.setState({ 
			select: false,
			search: true
		});
		
		actions.searchAddress(this.state.poi);  
	}

	renderSelect() {
		let addresses = [
			<a href="javascript:;" className="ui-list-group-item" key="current" onClick={this.positioning.bind(this)}>
				<i className="ui-icon ui-icon-location"></i>&nbsp;当前位置
			</a>
		];

		this.props.addresses.map((address) => {
			if (address.address == this.state.detail) return;

			addresses.push(
				<a key={address.uuid} href="javascript:;" className="ui-list-group-item" onClick={this.setAddress.bind(this, address.poi, address.address, address.longitude, address.latitude)}>
					<strong>{address.poi}</strong>
					<div className="text-muted text-sm">{address.address}</div>
				</a>
			)
		});

		return addresses;
	}

	renderSearch() {
		let { searchResults } = this.props;

		if (this.state.poi && searchResults == STATUS.process) {
			return <div className="ui-list-group-item text-muted">努力搜索中 ...</div>;
		} 

		if (searchResults.length) {
			let addresses = [];

			searchResults.map((address) => {
				addresses.push(
					<a href="javascript:;" className="ui-list-group-item" key={address.id} onClick={this.setAddress.bind(this, address.name, '', address.location.lng, address.location.lat)}>
						<strong>{address.name}</strong>
						&nbsp;&nbsp;<span className="text-muted text-sm">{address.address}</span>
					</a>
				);
			});

			return addresses;
		}

		if (this.state.poi && !searchResults.length) {
			return <div className="ui-list-group-item text-muted">无地址</div>;
		}
	}

	render() {
		let active = this.state.select || this.state.search;
		let addrClasses = classNames({
			'ui-list-group': true,
			'ui-affix': active
		});
		let detailClasses = classNames({
			'ui-list-group-item': true,
			'hide': active
		});

		return (
			<div className={addrClasses} ref="panel">
				<div className="ui-list-group-item ui-with-icon" style={{ height: '45px' }} onTouchEnd={this.toggle.bind(this, true, false)}>
					<i className="ui-icon ui-icon-map-marker"></i>&nbsp;
					{
						this.props.status == ORDERING_STATUS.init
							? <span className="text-muted">位置获取中 ...</span>
							: <input
									type="text"
									className="ui-form-ctrl ui-form-ctrl-block text-bold"
									value={this.state.poi}
									ref="poi"
									onChange={this.poiInput.bind(this)}
									onKeyUp={this.search.bind(this)} />
					}
				</div>
				<div className={detailClasses}>
					<div className="ui-input-group clearfix" style={{ height: '20px' }}>
						{
							this.props.status == ORDERING_STATUS.init
								? <span className="text-muted">地址获取中 ...</span>
								: !this.state.select
									? <input
											type="text"
											className="ui-form-ctrl ui-form-ctrl-block"
											value={this.state.detail}
											placeholder="详细送水地址（小区、单元、楼牌号）"
											ref="detail"
											onChange={this.detailInput.bind(this)}/>
									: null
						}
					</div>
				</div>
				{this.state.select ? this.renderSelect() : null}
				{this.state.search ? this.renderSearch() : null}
				{
					this.state.search && !this.state.poi
						? <div className="ui-list-group-item text-muted">请输入您要搜索的位置</div>
						: null
				}
				{active
					? <a href="javascript:;" className="ui-list-group-item text-primary" onClick={this.toggle.bind(this, false, false)}>收起</a>
					: null
				}
			</div>
		);
	}
}

OrderingAddress.propTypes = {
 	status: React.PropTypes.number,
  addresses: React.PropTypes.any,
  defaultAddress: React.PropTypes.any,
  searchResults: React.PropTypes.any,
  positioning: React.PropTypes.func
};

OrderingAddress.defaultProps = {
	status: ORDERING_STATUS.init,
	addresses: [],
	defaultAddress: {},
	searchResults: [],
	positioning: null
};
