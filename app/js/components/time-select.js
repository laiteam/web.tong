/**
 * components/time-select @lq 2015/07/31
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';

import { ORDERING_STATUS } from '../definitions';
import { timestr2timestamp } from '../libs/utils';
import ModalSelect from './modal-select';
import actions from '../actions/ordering';

export default class TimeSelect extends ModalSelect {

	constructor() {
		super();
	}

	getDays() {
		let { status } = this.props;

		if (status == ORDERING_STATUS.has) {
			return {
				0: '现在',
				1: '今天',
				2: '明天'
			};
		}

		if (status == ORDERING_STATUS.rest) {
			return {
				2: '明天'
			};
		}
	}

	getHours(options={start:0, end:0}) {
		let hours = { 0: '--' };
		let { start, end } = options;
		let startTimestamp = timestr2timestamp(start);
		let endTimestamp = timestr2timestamp(end);
		let startHour =new Date(startTimestamp).getHours();
		let endHour = new Date(endTimestamp).getHours();

		for (let i=startHour; i<=endHour; i++) {
			hours[i] = ((i < 10) ? '0'+i : i) + '点';
		}

		return hours;
	}

	getMinutes() {
		let minutes = { 0: '--&nbsp;&nbsp;' };

		for (let i of Array(6).keys()) {
			let k = !i ? '00' : String(i*10);
			minutes[i+1] = k + '分';
		}

		return minutes;
	}

	getSlotHourIdx(hour) {
		let values = SpinningWheel.slotData[1].values;
		let count = 0;

		for (let k in values) {
			if (hour == values[k].slice(0, 2)) return count;
			count ++
		}
	}

	getSlotMinutesIdx(minutes) {
		let m, v;

		for (let i of Array(6).keys()) {
			m = (!i ? '00' : String(i*10));
			if (m >= minutes) {
				v = i + 1;
				break;
			}
		}

		return v;
	}

	scrollTo(slotIdx, scrollIdx) {
		SpinningWheel.scrollTo(slotIdx, -SpinningWheel.cellHeight*scrollIdx);
	}

	getSelect() {
		let [ selectDay, _selectHour, _selectMinutes ] = SpinningWheel.getSelectedValues().values;
		let selectHour = Number(_selectHour.slice(0, 2));
		let selectMinutes = Number(_selectMinutes.slice(0, 2));

		return [ selectDay, selectHour, selectMinutes ];
	}

	valid(start, end) {
		let now = new Date();
		let nowHour = now.getHours();
		let nowMinutes = now.getMinutes();

		let startTimestamp = timestr2timestamp(start);
		let startDate = new Date(startTimestamp);
		let startHour = startDate.getHours();
		let startMinutes = !startDate.getMinutes() ? '00' : String(startDate.getMinutes());

		let endTimestamp = timestr2timestamp(end);
		let endDate = new Date(endTimestamp);
		let endHour = endDate.getHours();
		let endMinutes = !endDate.getMinutes() ? '00' : String(endDate.getMinutes());

		let [ selectDay, selectHour, selectMinutes ] = this.getSelect();

		if (selectDay == '现在') {
			this.slotVisible(0, 0);
		}

		if (selectDay == '今天') {
			let hour = nowHour;
			
			if (selectHour <= hour) {
				if (nowMinutes >= 50) {
					let nextHour = hour + 1;
					hour = (nextHour > endHour) ? endHour : nextHour;
					nowMinutes = 0;
				}
				
				let hourIdx = this.getSlotHourIdx(hour);
				this.scrollTo(1, hourIdx);
				this.slotVisible(hourIdx)

				let minuteIdx = this.getSlotMinutesIdx(nowMinutes);
				if (selectMinutes < nowMinutes) {
					this.scrollTo(2, minuteIdx);
				}
				this.slotVisible(hourIdx, minuteIdx);
			} else {
				this.slotVisible(null, 1);
			}
		}

		if (selectDay == '明天') {
			this.slotVisible(1, 1);
		}

		if (selectDay == '今天' || selectDay == '明天') {
			// some bug need refetch
			let [ selectDay, selectHour, selectMinutes ] = this.getSelect();

			if (selectHour == startHour) {
				let minuteIdx = this.getSlotMinutesIdx(startMinutes);
				if (selectMinutes < startMinutes) {
					this.scrollTo(2, minuteIdx)
				}
				this.slotVisible(null, minuteIdx)
			}

			if (selectHour == endHour) {
				let minuteIdx = this.getSlotMinutesIdx(endMinutes);
				if (selectMinutes > endMinutes) {
					this.scrollTo(2, minuteIdx)
				}
				this.slotVisible(null, minuteIdx, true)
			}
		}
	}

	done() {
		let [day, hour, minutes] = SpinningWheel.getSelectedValues().values;
		
		this.props.set(
			day == '现在'
				? '现在' 
				: `${day} ${Number(hour.slice(0, 2))}:${minutes.slice(0, 2)}` // 今天 7:10
		);

		if (day != '现在') actions.setSubmit(ORDERING_STATUS.rest);
		else actions.setSubmit(ORDERING_STATUS.has);
	}

	slotVisible(hourIdx, minuteIdx, reverse=false) {
		if (hourIdx != null) {
			let hourLis = document.querySelectorAll('.sw-33percent.sw-center > ul > li');
			for (let i=0,max=hourLis.length; i<max; i++) {
				hourLis[i].style.visibility = 'visible';
				if (!hourIdx && i)
					hourLis[i].style.visibility = 'hidden';
				if (hourIdx && i < hourIdx)
					hourLis[i].style.visibility = 'hidden';
			}
		}

		if (minuteIdx != null) {
			let minuteLis = document.querySelectorAll('.sw-33percent.sw-right ul li');
			for (let i=0,max=minuteLis.length; i<max; i++) {
				minuteLis[i].style.visibility = 'visible';
				if (!minuteIdx && i)
					minuteLis[i].style.visibility = 'hidden';

				if (minuteIdx && !reverse) {
					if (i < minuteIdx)
						minuteLis[i].style.visibility = 'hidden';
				} 

				if (minuteIdx && reverse) {

					if (!i || i > minuteIdx)
						minuteLis[i].style.visibility = 'hidden';
				}
			}
		}
	}

	slot() {
		let { status, start, end } = this.props;

		SpinningWheel.addSlot(this.getDays(), '33percent left');

		SpinningWheel.addSlot(this.getHours({
			start: start,
			end: end
		}), '33percent center');

		SpinningWheel.addSlot(this.getMinutes(), '33percent right');
		
		SpinningWheel.setCancelAction(() => {
			this.close();
		});

		SpinningWheel.setDoneAction(() => {
			this.done();
		});

		SpinningWheel.scrollOver = () => {
			let [day, hour, minute] = SpinningWheel.getSelectedValues().keys;
			
			// 如果是“现在”，小时和分钟必须是“--“
			if (!Number(day)) {
				this.scrollTo(1, 0);
				this.scrollTo(2, 0);
			} else {
				// 如果是“今天”，则不能选择“--“
				if (!Number(hour)) {
					this.scrollTo(1, 1);
				} 

				// 如果是“明天”，则不能选择“--“
				if (!Number(minute)) {
					this.scrollTo(2, 1);
				}
			}

			this.valid(start, end);
		};

		SpinningWheel.open('选择时间');

		if (status == ORDERING_STATUS.has)
			this.slotVisible(0, 0);

		if (status == ORDERING_STATUS.rest) {
			this.scrollTo(1, 1);
			this.scrollTo(2, 1);
			this.slotVisible(1, 1);			
		}
	}
}

TimeSelect.defaultProps = {
	start: 0,
	end: 0,
	status: ORDERING_STATUS.init
};

TimeSelect.propTypes = {
	start: React.PropTypes.any,
	end: React.PropTypes.any,
	status: React.PropTypes.number
}