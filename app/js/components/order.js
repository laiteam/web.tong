/**
 * components/order @lq 2015/08/02
*/

'use strict';

import React from 'react';
import reactMixin from 'react-mixin';
import { Navigation } from 'react-router';
import classNames from 'classnames';

import { supportLocalStorage } from '../libs/utils';
import storage from '../libs/storage';
import { ORDER_STATUS, STORAGE_KEY } from '../definitions';

@reactMixin.decorate(Navigation)
export default class Order extends React.Component {

	constructor(props) {
		super(props);
	}

	detail() {
		// 若支持 storage 则用 jwt cache it
		// if (supportLocalStorage()) {
		// 	storage(STORAGE_KEY.order, this.props, { jwt: true });
		// }

		this.transitionTo('order/:id', { id: this.props.id });
	}

	componentDidMount() {
		React.findDOMNode(this).addEventListener('click', (e) => {
			this.props.status != -1 && this.detail();
		});
	}

	render() {
		let { key, deliveredAt, status, addr, wares } = this.props;
		let ware = wares[0];

		let classes = classNames({
			'ui-panel ui-panel-sm ui-db': true,
			'ui-panel-muted': status == ORDER_STATUS.cancel,
			'ui-mt-10': !key // idx from list
		});

		return (
			<div className={classes}>
				<div className="ui-panel-header">
					{`${deliveredAt.month}月${deliveredAt.day}日`}&nbsp;<span className="text-muted">{`${deliveredAt.hour}:${deliveredAt.minute}`}</span>
					{
						() => {
							switch (true) {
								case status == ORDER_STATUS.unpay: return <span className="pull-right text-danger">待付款</span>
								case status == ORDER_STATUS.undelivery: return <span className="pull-right text-success">待配送</span>;
								case status == ORDER_STATUS.done: return <span className="pull-right text-muted">已完成</span>;
								case status == ORDER_STATUS.cancel: return <span className="pull-right">已取消</span>;
							}
						}()
					}
				</div>
				<div className="ui-panel-body">
					<div className="text-center ui-pos-re">
						<span className="ui-pos-ab -left">{ware.name}</span>
						<span>x{ware.count}</span>
						<span className="ui-pos-ab -right">¥{ware.price}</span>
						{/* this.props.deposited 
							  ? <div className="text-left ui-mt-10">
							  		<span className="pull-right">¥{this.props.deposit}</span>
							  		水桶押金
							  	</div> 
							  : null 
						*/}
					</div>
					<div className="ui-mt-10">{addr}</div>
				</div>
			</div>
		);
	}
}

Order.defaultProps = {
	id: '',
	status: 0,
	deliveredAt: {},
	addr: '',
	packman: {},
	wares: []
};

Order.propTypes = {
	id: React.PropTypes.string,
	status: React.PropTypes.number,
	deliveredAt: React.PropTypes.object,
	addr: React.PropTypes.string,
	packman: React.PropTypes.object,
	wares: React.PropTypes.array
};