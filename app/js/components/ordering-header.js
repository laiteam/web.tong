/**
 * components/ordering-header.js @lq 2015/07/22
*/

'use strict';

import React from 'react';

import { ORDERING_STATUS } from '../definitions';

export default class OrderingHeader extends React.Component {

	constructor(props) {
		super(props);		
	}

	componentDidMount() {
		// 模拟 data inject
		// let st = window.setTimeout(() => {
		// 	this.setState({ fetched: true });
		// 	window.clearTimeout(st);
		// }, 1000);
	}

	render() {
		let { status, packman } = this.props;

		return (
			<p className="text-center">
				{
					status == ORDERING_STATUS.init
						? <span className="text-muted">水站获取中 ...</span>
						: <span>
								{() => {
									switch (status) {
										case ORDERING_STATUS.has: return <span className="text-success">您附近有水站可提供服务</span>;
										case ORDERING_STATUS.no: return <span className="text-danger">对不起，您所在的位置暂时无法提供服务</span>;
										case ORDERING_STATUS.rest: return <span className="text-danger">水站休息中，营业时间 {packman.start_at}-{packman.end_at}</span>;
									}
								}()}
							</span>
				}
			</p>
		);
	}
}

OrderingHeader.defaultProps = {
	status: ORDERING_STATUS.init,
	packman: ORDERING_STATUS.init
};

OrderingHeader.propTypes = {
	status: React.PropTypes.number,
	packman: React.PropTypes.any
};