/**
 * components/modal-select @lq 2015/07/31
*/

'use strict';

import React from 'react';
import classNames from 'classnames';

export default class ModalSelect extends React.Component {

	constructor(props) {
		super(props);
	}

	close() {
		this.props.toggle(false);	
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.show) {
			this.slot();
		}
	}

	render() {
		let classes = classNames({
			'ui-modal-select': true,
			'in': this.props.show
		});

		return (
			<div className={classes}></div>
		);
	}
}

ModalSelect.defaultProps = { 
	show: false,
	toggle: null,
	set: null
};

ModalSelect.propTypes = { 
	show: React.PropTypes.bool,
	toggle: React.PropTypes.func,
	set: React.PropTypes.func
};