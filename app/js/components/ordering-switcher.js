/**
 * components/ordering-switcher @lq 2015/07/27
 *
 * props.payType: wechat 微信 / ticket 水票
*/

'use strict';

import React from 'react';

export default class OrderingSwitcher extends React.Component {

	constructor (props) {
		super(props);
	}

	render() {
		let toggle = this.props.toggle;

		return (
			<div className="ui-slide-switcher">
				<a href="javascript:;" className={this.props.payType == 'wechat' ? 'ui-slide-switcher-checked' : ''} onClick={() => toggle('wechat')}>微信支付</a>
				<div className="ui-slide-switcher-inner">
					<a href="javascript:;" className="ui-slide-switcher-left" onClick={() => toggle('wechat')}></a>
					<a href="javascript:;" className={'ui-slide-switcher-toggle ui-toggle-' + (this.props.payType == 'wechat' ? 'left' : 'right')}></a>
					<a href="javascript:;" className="ui-slide-switcher-right" onClick={() => toggle('ticket')}></a>
				</div>
				<a href="javascript:;" className={this.props.payType == 'ticket' ? 'ui-slide-switcher-checked' : ''} onClick={() => toggle('ticket')}>纸质水票</a>
			</div>
		);
	}
}

OrderingSwitcher.propTypes = {
	payType: React.PropTypes.string,
	toggle: React.PropTypes.func
};

OrderingSwitcher.defaultProps = { 
	payType: 'wechat',
	toggle: null
};