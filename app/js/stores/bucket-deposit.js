/**
 * stores/bucket-deposit.js created by lq on 15/07/20
*/

'use strict';

import Reflux from 'reflux';
import actions from '../actions/bucket-deposit';

let store = Reflux.createStore({
	listenables: actions
});

export default store;