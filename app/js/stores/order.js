/**
 * stores/order @lq 2015/08/04
*/

'use strict';

import Reflux from 'reflux';
import reactMixin from 'react-mixin';

import storage from '../libs/storage';
import { ajax, req } from '../libs/request';
import date from '../libs/date';
import store from '../decorators/store';
import { API, STATUS, COUNT } from '../definitions';
import actions from '../actions/order';

export default Reflux.createStore({
	mixins: [ store ],

	listenables: actions,

	init() {
		this.state = {
			orders: STATUS.process,
			order: STATUS.process
		};
	},

	parse(order) {
		let obj = {
			id: String(order.uuid),
			status: Number(order.status),
			deliveredAt: date(order.delivered_at),
			addr: order.address,
			packman: order.packman,
			wares: order.wares
		};

		return obj;
	},

	parses(orders) {
		let list = [];

		orders.map((order) => {
			list.push(this.parse(order));
		});

		return list;
	},

	onFetchAll() {
		ajax(function * () {
			let [ stat, res ] = yield req(API.orders, { count: COUNT });

			this.setState({ orders: !stat ? [] : this.parses(res.orders) });
		}.bind(this));
	},

	onFetchOne(oId) {
		ajax(function * () {
			let [ stat, res ] = yield req(API.order, { order_id: oId });

			this.setState({ order: stat ? this.parse(res) : STATUS.fail });
		}.bind(this));
	},

	onCancel(oId) {
		ajax(function * () {
			let [ stat, res ] = yield req(API.cancel, { order_id: oId });

			if (stat) this.setState({ order: 'cancel' });
		}.bind(this));
	}
});