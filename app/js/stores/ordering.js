/**
 * stores/ordering @lq 2015/08/07
*/

'use strict';

import Reflux from 'reflux';

import { ajax, req } from '../libs/request';
import amap from '../libs/amap';
import { timestr2timestamp } from '../libs/utils';
import { API, STATUS, ORDERING_STATUS, COUNT, DEBOUNCE } from '../definitions';
import store from '../decorators/store';
import actions from '../actions/ordering';

// Beta
import ADDRESSES from '../data/addresses';

export default Reflux.createStore({
  mixins: [ store ],

  listenables: actions,

  init() {
    this.state = {
      status: ORDERING_STATUS.init,
      submit: ORDERING_STATUS.init,
      addresses: STATUS.init,
      defaultAddress: STATUS.init,
      searchResults: STATUS.init,
      packman: STATUS.init,
      wares: STATUS.init,
      lng: 0, // 用户当前纬度
      lat: 0, // 用户当前经度
      cityCode: '', // 用户当前所在城市编码
      start: 0,
      end: 0
    };

    this.searchLast = null;
  },

  // 验证时间
  validTime({ start_at: start, end_at: end }) {
    console.log(start);
    console.log(end);

    start = timestr2timestamp(start);
    end = timestr2timestamp(end);
    let now = Date.now();    

    if (now >= start && now <= end) {
      // 在营业时间内
      console.log('在营业时间内');
      return ORDERING_STATUS.has;
    } else {
      // 水站休息
      console.log('水站休息');
      return ORDERING_STATUS.rest;
    }
  },

  // 获取地址信息
  getPoi(lng, lat, cb) {
    console.log('getPoi');
    amap.getPoi(lng, lat, (stat, poi, cityCode) => {
      if (stat) {
        // 找到Poi
        cb(true, poi, cityCode, lng, lat);
      } else {
        // 没有找到Poi
        alert('没有找到Poi');
        cb(false);
      }
    });  
  }, 

  // 定位
  positioning(cb) {
    console.log('positioning');
    
    let getPoiCB = (stat, poi, cityCode, lng, lat) => {
      if (stat) {
        cb(true, poi, cityCode, lng, lat);
      } else {
        cb(false);
      }
    };

    amap.getPosition((stat, res) => {
      if (stat) {
        let { lng, lat } = res.position;

        this.getPoi(lng, lat, getPoiCB);
      } else {
        cb(false);
      }
    });
  },

  onFetchAddress() {
    ajax(function * () {
      let [ stat, res ] = yield req(API.address);
      let addresses = stat ? res : [];

      this.setState({ 
        addresses: addresses,
        defaultAddress: addresses.length ? addresses[0] : false
      });
    }.bind(this));
  },

  onSearchAddress(keywords) {
    if (!keywords) {
      clearTimeout(this.searchLast);
      setTimeout(() => {
        this.setState({ searchResults: [] });  
      }, DEBOUNCE.searchAddress/4);
      return
    }

    this.setState({ searchResults: STATUS.process });

    if (this.searchLast) clearTimeout(this.searchLast);

    this.searchLast = setTimeout(() => {
      console.log('onSearchAddress');  
      let { cityCode } = this.state;
      let placeSearchCB = (stat, res) => {
        if (stat) {
          // 有结果
          this.setState({ searchResults: res.poiList.pois });
        } else {
          // 无结果
          this.setState({ searchResults: [] });
        }
      };

      if (!cityCode) {
        // 定位获取当前city
        this.positioning((stat, poi, cityCode) => {
          if (stat) {
            amap.placeSearch(cityCode, keywords, placeSearchCB);
          } else {
            alert('获取 cityCode 失败');
          }
        });
      } else {
        amap.placeSearch(cityCode, keywords, placeSearchCB);
      }
    }, DEBOUNCE.searchAddress)    
  },

  onPositioning() {    
    this.positioning((stat, poi, cityCode, lng, lat) => {
      if (stat) {
        this.setState({
          lng: lng,
          lat: lat,
          cityCode: cityCode,
          defaultAddress: {
            poi: poi,
            longitude: lng,
            latitude: lat
          }
        });
      } else {
        alert('定位失败');
      }
    });
  },

  onSearchPackman() {
    let { longitude: lng, latitude: lat } = this.state.defaultAddress;
  
    amap.searchNearBy(lng, lat, (stat, packman) => {
      if (stat) {
        // 附近有水站，验证营业时间
        let orderingStatus = this.validTime(packman);

        this.setState({ 
          status: orderingStatus,
          submit: orderingStatus,
          packman: packman 
        });
      } else {
        // 附近无水站
        this.setState({ 
          status: ORDERING_STATUS.no,
          submit: ORDERING_STATUS.no
        });
      }
    });

    this.setState({ packman: STATUS.process });
  },

  onFetchWares(packmanId) {
    ajax(function * () {
      let [ stat, res ] = yield req(API.wares, { 
        packman_id: packmanId,
        count: COUNT
      });

      this.setState({ wares: stat ? res : [] });
    }.bind(this));
  },

  onSelect(poi, lng, lat) {
    this.setState({
      status: ORDERING_STATUS.init,
      packman: STATUS.init,
      wares: STATUS.init,
      defaultAddress: {
        poi: poi,
        longitude: lng,
        latitude: lat
      }
    });
  },

  onReset() {
    this.setState({
      status: ORDERING_STATUS.init,
      submit: ORDERING_STATUS.init,
      defaultAddress: STATUS.init,
      packman: STATUS.init,
      wares: STATUS.init
    });
  },

  onSetStatus(status) {
    this.setState({ status: status });
  },

  onSetSubmit(submit) {
    this.setState({ submit: submit });
  }
});
