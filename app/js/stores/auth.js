/**
 * stores/auth @lq 2015/08/03
*/

'use strict';

import Reflux from 'reflux';
import reactMixin from 'react-mixin';

import storage from '../libs/storage';
import { ajax, req } from '../libs/request';
import store from '../decorators/store';
import { API, STATUS, COOKIE, STORAGE_KEY } from '../definitions';
import actions from '../actions/auth';

export default Reflux.createStore({
	mixins: [ store ],

	listenables: actions,

	init() {
		this.state = {
			captcha: STATUS.init,
			login: STATUS.init
		};
	},

	onFetchCaptcha(phone) {
		ajax(function * () {
			let [ stat, res ] = yield req(API.verification, { phone: phone });

			if (!stat) this.setState({ captcha: STATUS.fail });
		}, { alert: true });
	},

	onLogin(phone, captcha) {
		ajax(function * () {
			let [ stat, res ] = yield req(API.login, {
				phone: phone,
				verification: captcha
			});

			if (!stat) this.setState({ login: STATUS.fail });
			else {
				let storageConf = {
					hashids: true,
					expires: COOKIE.getInfinite()
				};
				
				storage.set(STORAGE_KEY.uid, res.uuid, storageConf);
				storage.set(STORAGE_KEY.phone, res.phone, storageConf);

				this.setState({ login: STATUS.done });
			}
		}.bind(this), { alert: true });
	},

	logout() {
		storage.remove(STORAGE_KEY.uid);
		storage.remove(STORAGE_KEY.phone);
	},

	checkLogined() {
		return storage.get(STORAGE_KEY.uid, { hashids: true });
	}
});