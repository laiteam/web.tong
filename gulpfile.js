'use strict';

var gulp = require('gulp');
var rs = require('run-sequence');
var $ = require('gulp-load-plugins')();

var rd = require('require-dir');
rd('gulp_tasks/');

gulp.task('ui', ['clean:ui'], function() {
	rs(
		['extend', 'style'],
		['server:ui', 'watch']
	);
});

gulp.task('default', ['clean'], function() {
	rs(
		'cssmin',
		['script', 'html', 'img'],
		'rev',
		'publish',
		['server', 'watch']
	);
})