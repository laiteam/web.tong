'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var del = require('del');

gulp.task('clean:ui', function() {
	del('ui/outputs/*.html', { read: false });
});

gulp.task('clean:css', function() {
	del('dist/css/', { read: false });
});

gulp.task('clean', function() {
	del([
			'dist/',
			'app/css/'
	], { read: false });
});