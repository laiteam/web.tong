'use strict';

var gulp = require('gulp');

gulp.task('css', function() {
  return gulp.src('ui/css/base.min.css')
    .pipe(gulp.dest('dist/css/'));
});