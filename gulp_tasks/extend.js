'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var extender = require('gulp-html-extend');

gulp.task('extend', function() {
	return gulp
		.src([
			'ui/components/*.html',
			'ui/pages/*.html'
		])
		.pipe(extender({
			annotations: false,
			verbose: false
		}))
		.pipe(gulp.dest('ui/outputs/'))
		.pipe($.connect.reload());
});