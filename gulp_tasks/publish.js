'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

gulp.task('publish', function() {
	return gulp.src([
			'rev/**/*.json',
			'dist/index.html'
		])
		.pipe($.revCollector())
		.pipe(gulp.dest('dist/'))
		.pipe($.connect.reload());
});