'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var ms = require('merge-stream');

// set variable via $ gulp --type pro
var env = $.util.env.type || 'dev';
var isPro = env === 'pro';

var webpackConfig = require('../webpack.config.js').getConfig(env);

gulp.task('script', function() {
  return ms(
  	gulp.src('src/entry.js')
    	.pipe($.webpack(webpackConfig))
    	.pipe(isPro ? $.uglifyjs() : $.util.noop())
    	.pipe(gulp.dest('dist/js/'))
      .pipe($.connect.reload())
    ,
    gulp.src([
        'app/js/libs/spinningwheel.js'
      ])
    	.pipe($.concat('libs.min.js'))
    	.pipe($.uglifyjs())
    	.pipe(gulp.dest('dist/js/'))
   );
});