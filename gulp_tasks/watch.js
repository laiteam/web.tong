'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var rs = require('run-sequence');

gulp.task('watch', function() {
	gulp.watch([
		'ui/master.html',
		'ui/components/*.html',
		'ui/pages/*.html',
	], ['extend']);

	gulp.watch([
		'ui/**/*.less',
		'!ui/components/_sprite.less',
		'!ui/tmp/*.less'
	], function() {
		rs(
			['style', 'html'],
			'cssmin',
			'rev',
			'publish'
		);
	});

	gulp.watch([
		'app/js/*.js',
		'app/js/**/*.js'
	], ['script']);
});