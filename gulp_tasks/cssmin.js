'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

gulp.task('cssmin', function() {
  return gulp
    .src('ui/css/base.css')
    .pipe($.autoprefixer({ 
      browsers: [ 'last 2 versions' ] 
    }))
    .pipe($.rename({ suffix: '.min' }))
    .pipe($.minifyCss({ keepSpecialComments: false }))
    .pipe(gulp.dest('ui/css/'));
});