'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var sprite = require('css-sprite').stream;

gulp.task('sprite', function() {
	return gulp
		.src('ui/img/icons/*.png')
		.pipe(sprite({
			name: 'sprite',
			style: '_sprite.less',
			processor: 'less',
			prefix: 'ui-icon',
			cssPath: '../img/',
			template: 'ui/img/icons/less.mustache'
		}))
		.pipe($.if('*.png', gulp.dest('ui/img/'), gulp.dest('ui/components/')));
});