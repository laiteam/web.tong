'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var ms = require('merge-stream');

gulp.task('rev', ['clean:css'], function() {
	return ms(
		gulp.src('ui/css/*.min.css')
			.pipe($.rev())
			.pipe(gulp.dest('dist/css/'))
			.pipe($.rev.manifest())
			.pipe(gulp.dest('rev/css/'))
	);
});