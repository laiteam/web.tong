'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

gulp.task('server:ui', function() {
	$.connect.server({
		root: 'ui/',
		host: '0.0.0.0',
		port: 8000,
		livereload: true
	});
});

gulp.task('server', function() {
	$.connect.server({
		root: 'dist/',
		host: '0.0.0.0',
		port: 8000,
		livereload: true,
		//fallback: 'index.html'
	});
});