'use strict';

var path = require('path');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

gulp.task('style', function() {
	return gulp
		.src([
			'node_modules/bootstrap/less/variables.less',
			'node_modules/bootstrap/less/mixins.less',
			'node_modules/bootstrap/less/normalize.less',
			'node_modules/bootstrap/less/scaffolding.less',
			'node_modules/bootstrap/less/utilities.less',
			'ui/core/variables.less',
			'ui/core/mixins.less',
			'ui/core/base.less',
			'ui/core/typography.less',
			'ui/core/grid.less',
			'ui/core/utils.less',
			'ui/components/*.less',
			'!ui/components/_sprite.less' // has imported by icons.less
		])
		.pipe($.concat('base.less'))
		.pipe(gulp.dest('ui/tmp/'))
		.pipe($.less({
			paths: path.join(__dirname, '../node_modules/bootstrap/less/')
		}))
		.pipe($.autoprefixer({
			browsers: [ 
				'ie >= 9',
				'ie_mob >= 10',
				'ff >= 30',
				'chrome >= 34',
				'safari >= 6',
				'opera >= 23',
				'ios >= 6',
				'android >= 4.4',
				'bb >= 10'
			] 
		}))
		.pipe(gulp.dest('ui/css/'));
});