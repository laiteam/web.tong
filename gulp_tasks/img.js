'use strict';

var gulp = require('gulp');

gulp.task('img', function() {
  return gulp.src('ui/img/*.{png,jpg,jpeg,gif}')
    .pipe(gulp.dest('dist/img/'));
});